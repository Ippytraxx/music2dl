#ifndef SONGMODEL_H
#define SONGMODEL_H
#include "models/ListItem.h"

class SongModel : public Models::ListItem
{
    Q_OBJECT

    Q_PROPERTY(QString artist WRITE setArtist)
    Q_PROPERTY(QString title WRITE setTitle)
    Q_PROPERTY(QString link WRITE setLink)

    public:
        explicit SongModel(QObject *parent = NULL);
        explicit SongModel(int id, const QString &artist, const QString &title, QObject *parent = NULL);
        explicit SongModel(int id, const QString &artist, const QString &title, const QString &link, QObject *parent = NULL);

        int id() const;
        QVariant data(int role) const;
        bool setData(int role, const QVariant &value);
        QHash<int, QByteArray> roleNames() const;

        void setArtist(const QString &artist);
        void setTitle(const QString &title);
        void setLink(const QString &link);

        QString getArtist() const;
        QString getTitle() const;
        QString getLink() const;

        enum SongRoles
        {
            artist_role = Qt::UserRole + 1,
            title_role,
            link_role
        };

    private:
        int songId;
        QString artist;
        QString title;
        QString link;
};
#endif
