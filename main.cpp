#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlPropertyMap>
#include <QtQml>
#include <QDebug>
#include <QStandardPaths>

#include "models/ListModel.h"
#include "models/ListItem.h"
#include "model.h"
#include "songstorage.h"

#include <iostream>

QQmlPropertyMap myMap;
SongStorage myStorage(&myMap);

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<SongModel>("org.bitbucket.music2dl", 1, 0, "Song");

    myStorage.loadSongs();

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("songMap", &myMap);

    engine.load(QUrl(QStringLiteral("qrc:view1.qml")));

    QObject *saveButton = engine.rootObjects().first()->findChild<QObject*>("saveButton");
    QObject::connect(saveButton, SIGNAL(saveSignal()), &myStorage, SLOT(saveSongs()));

    qDebug() << QStandardPaths::writableLocation(QStandardPaths::DataLocation);

    return app.exec();
}
