#include "model.h"

SongModel::SongModel(QObject *parent) : Models::ListItem(parent)
{
}

SongModel::SongModel(int id, const QString &artist, const QString &title, QObject *parent) : Models::ListItem(parent), songId(id), artist(artist), title(title)
{
}

SongModel::SongModel(int id, const QString &artist, const QString &title, const QString &link, QObject *parent) : Models::ListItem(parent), songId(id), artist(artist), title(title), link(link)
{
}

int SongModel::id() const
{
    return songId;
}

QVariant SongModel::data(int role) const
{
    switch(role)
    {
        case artist_role:
            return artist;
        case title_role:
            return title;
        case link_role:
            return link;
        default:
            QVariant();
    }
}

bool SongModel::setData(int role, const QVariant &value)
{
    switch(role)
    {
        case artist_role:
            artist = value.toString();
            triggerItemUpdate();
            return true;
        case title_role:
            title = value.toString();
            triggerItemUpdate();
            return true;
        case link_role:
            link = value.toString();
            triggerItemUpdate();
            return true;
        default:
            return false;
    }
}

QHash<int, QByteArray> SongModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[artist_role] = "artist";
    roles[title_role] = "title";
    roles[link_role] = "link";

    return roles;
}

void SongModel::setArtist(const QString &artist)
{
    this->artist = artist;
}

void SongModel::setTitle(const QString &title)
{
    this->title = title;
}

void SongModel::setLink(const QString &link)
{
    this->link = link;
}

QString SongModel::getArtist() const
{
    return artist;
}

QString SongModel::getTitle() const
{
    return title;
}

QString SongModel::getLink() const
{
    return link;
}
