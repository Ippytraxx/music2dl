import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import org.bitbucket.music2dl 1.0

ApplicationWindow
{
    visible: true
    height: 480
    width: 200

    function addSong()
    {
        console.log("Adding song")
        var new_song1 = Qt.createQmlObject("import org.bitbucket.music2dl 1.0; Song{}", songMap[listSelector.currentText])
        new_song1.artist = artistField.text
        new_song1.title = titleField.text
        new_song1.link = linkField.text

        songMap[listSelector.currentText].appendRowFromQml(new_song1)
        console.log("List size is: " + songMap[listSelector.currentText].count)

        artistField.text = ""
        titleField.text = ""
        linkField.text = ""
    }

    function removeSong()
    {
        console.log("Removing song")
        console.log(songView.currentIndex)
        songMap[listSelector.currentText].removeRowFromQml(songView.currentIndex)
    }

    RowLayout
    {
        id: top
        spacing: 10

        ComboBox
        {
            id: listSelector
            model: songMap.keys()
            onCurrentTextChanged: songView.model = songMap[listSelector.currentText]
        }

        Button
        {
            id: openAddSong
            text: "Add"
            onClicked: addSongPopup.visible = true
        }
    }

    Item 
    {
        id: addSongPopup
        anchors.centerIn: parent
        width:  addSongBg.width  + (2 * addSongBgShadow.radius)
        height: addSongBg.height + (2 * addSongBgShadow.radius)
        visible: false
        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2
        z: 2

        Rectangle 
        {
            id: addSongBg
            width: songDetails.width + 20
            height: songDetails.height + 20
            color: "white"
            radius: 7
            antialiasing: true
            anchors.centerIn: parent
        }
        ColumnLayout
        {
            id: songDetails
            spacing: 10 
            height: 150
            width: 200

            anchors.centerIn: parent

            TextField
            {
                id: artistField
                placeholderText: "Artist"     
                Layout.fillWidth: true
            }
            TextField
            {
                id: titleField
                placeholderText: "Title"
                Layout.fillWidth: true
            }
            TextField
            {
                id: linkField
                placeholderText: "Link"
                Layout.fillWidth: true
            }

            RowLayout
            {
                spacing: 10

                Layout.fillWidth: true

                Button
                {
                    text: "Done"
                    Layout.fillWidth: true

                    onClicked: 
                    {
                        addSongPopup.visible = false
                        addSong()
                    }
                }
                Button
                {
                    text: "Cancel"
                    Layout.fillWidth: true
                    onClicked: addSongPopup.visible = false
                }
            }
        }
    }
    DropShadow 
    {
        id: addSongBgShadow;
        anchors.fill: source
        cached: true;
        radius: 16;
        samples: 16;
        color: "#80000000";
        smooth: true;
        source: addSongPopup;
        visible: addSongPopup.visible
    }

    /*Item*/
    /*{*/
        /*id: addSongPopup*/
        /*height: addSongBg.height + 2 * addSongBg.radius*/
        /*width: addSongBg.width + 2 * addSongBg.radius*/
        /*visible: false*/
        /*x: parent.width / 2 - width / 2*/
        /*y: parent.height / 2 - height / 2*/
        /*z: 2*/

        /*Rectangle*/
        /*{*/
            /*id: addSongBg*/
            
            /*anchors.centerIn: parent*/
            /*width: 100*/
            /*height: 100*/
            /*visible: false*/
            /*color: "white"*/
            /*radius: 3*/

        /*}*/

        /*DropShadow*/
        /*{*/
            /*id: bgShadow*/
            /*cached: true*/
            /*smooth: true*/
           /*anchors.fill: source*/
           /*[>width: addSongBg.width + 50<]*/
           /*[>height: addSongBg.height + 50<]*/
           /*radius: 16*/
           /*samples: 16*/
           /*color: "#80000000"*/
           /*source: addSongBg*/
        /*}*/

        /*ColumnLayout*/
        /*{*/
            /*id: songDetails*/
            /*spacing: 10 */

            /*anchors.centerIn: parent*/

            /*TextField*/
            /*{*/
                /*id: artistField*/
                /*placeholderText: "Artist"     */
                /*Layout.fillWidth: true*/
            /*}*/
            /*TextField*/
            /*{*/
                /*id: titleField*/
                /*placeholderText: "Title"*/
                /*Layout.fillWidth: true*/
            /*}*/
            /*TextField*/
            /*{*/
                /*id: linkField*/
                /*placeholderText: "Link"*/
                /*Layout.fillWidth: true*/
            /*}*/

            /*RowLayout*/
            /*{*/
                /*spacing: 10*/

                /*Button*/
                /*{*/
                    /*text: "Done"*/

                    /*onClicked: */
                    /*{*/
                        /*addSongPopup.visible = false*/
                        /*addSong()*/
                    /*}*/
                /*}*/
                /*Button*/
                /*{*/
                    /*text: "Cancel"*/
                    /*onClicked: addSongPopup.visible = false*/
                /*}*/
            /*}*/
        /*}*/
    /*}*/

    ListView
    {
        id: songView
        model: songMap["chill"]
        spacing: 20
        focus: true

        highlight: Rectangle{ color: "lightsteelblue"; radius: 3}
        
        anchors.top: top.bottom
        anchors.bottom: bottom.top
        anchors.left: parent.left
        anchors.right: parent.right

        delegate:
            Item
            {
                width: songView.width
                height: 40
                Keys.onPressed:
                {
                    if(event.key == Qt.Key_Delete)
                    {
                        removeSong()
                    }
                }
                MouseArea
                {
                    anchors.fill: parent
                    z: 2
                    onClicked:
                    {
                        console.log("Clicked")
                        songView.currentIndex = index
                        parent.forceActiveFocus()
                    }
                    onDoubleClicked:
                    {
                        Qt.openUrlExternally(link)
                    }
                }
                Row
                {
                    spacing: 10
                    anchors.verticalCenter: parent.verticalCenter

                    Text
                    {
                        text: artist
                        font.bold: true
                    }
                    Text
                    {
                        text: title
                    }
                }
            }
    }
    Row
    {
        id: bottom
        spacing: 10
        anchors.bottom: parent.bottom
        Button
        {
            id: saveButton
            objectName: "saveButton"
            text: "Save"

            signal saveSignal()

            onClicked: 
            {
                saveButton.saveSignal()
            }
        }
        Button
        {
            id: quitButton
            text: "Quit"
    
            onClicked: Qt.quit()
        }
    }
}
