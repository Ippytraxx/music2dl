import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import org.bitbucket.music2dl 1.0

ApplicationWindow
{
    visible: true
    height: 480
    width: 200

    function addSong()
    {
        var new_song1 = Qt.createQmlObject("import org.bitbucket.music2dl 1.0; Song{}", songMap[listSelector.currentText])
        new_song1.artist = artistField.text
        new_song1.title = titleField.text
        new_song1.link = linkField.text

        songMap[listSelector.currentText].appendRowFromQml(new_song1)
        console.log("List size is: " + songMap[listSelector.currentText].count)

        artistField.text = ""
        titleField.text = ""
        linkField.text = ""
    }

    function removeSong()
    {
        console.log("Removing song")
        console.log(songListView.currentIndex)
        songMap[listSelector.currentText].removeRowFromQml(songListView.currentIndex)
    }

    Row
    {
        id: addBox
        spacing: 5

        ComboBox
        {
            id: listSelector
            /*anchors.fill: parent*/
            model: songMap.keys()
            onCurrentTextChanged: songListView.model = songMap[listSelector.currentText]
        }

        Button
        {
            id: openAddSong
            text: "Add"
            onClicked: songDetails.state = "showing"
        }

    }
    ColumnLayout
    {
        id: songDetails
        /*anchors.fill: parent*/
        anchors.top: addBox.bottom
        spacing: 10
        Keys.onReturnPressed: state = ""
        height: 0
        states: 
        [
            State
            {
                name: "showing"
                PropertyChanges
                {
                    target: songDetails
                    height: 100
                }
            }
        ]
        transitions:
        [
            Transition
            {
                NumberAnimation
                {
                    target: songDetails
                    properties: "height"
                }
            }
        ]
        RowLayout
        {
            spacing: 10
            height: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            TextField
            {
                Layout.fillWidth: true
                Layout.fillHeight: true
                id: artistField
                placeholderText: "Artist"     
            }
            TextField
            {
                Layout.fillWidth: true
                Layout.fillHeight: true
                id: titleField
                placeholderText: "Title"
            }
        }

        TextField
        {
            id: linkField
            placeholderText: "Link"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
    ListView
    {
        id: songListView
        model: songMap["chill"]
        spacing: 20
        highlight: Rectangle{ color: "lightsteelblue"; radius: 3}
        focus: true

        anchors.top: songDetails.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        
        delegate:
            Item
            {
                width: songListView.width
                height: 40
                Keys.onPressed:
                {
                    if(event.key == Qt.Key_Delete)
                    {
                        removeSong()
                    }
                }
                MouseArea
                {
                    anchors.fill: parent
                    z: 2
                    onClicked:
                    {
                        console.log("Clicked")
                        songListView.currentIndex = index
                        parent.forceActiveFocus()
                    }
                    onDoubleClicked:
                    {
                        Qt.openUrlExternally(link)
                    }
                }
                Row
                {
                    spacing: 10
                    anchors.verticalCenter: parent.verticalCenter

                    Text
                    {
                        text: artist
                        font.bold: true
                    }
                    Text
                    {
                        text: title
                    }
                }
            }
    }
    Row
    {
        spacing: 10
        anchors.bottom: parent.bottom
        Button
        {
            id: saveButton
            objectName: "saveButton"
            text: "Save"

            signal saveSignal()

            onClicked: 
            {
                saveButton.saveSignal()
            }
        }
        Button
        {
            id: quitButton
            text: "Quit"
    
            onClicked: Qt.quit()
        }
    }
}
