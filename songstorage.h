#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QFile>
#include <QIODevice>
#include <QQmlPropertyMap>
#include <QStandardPaths>
#include <QDir>
#include "models/ListModel.h"
//#include "models/ListItem.h"
#include "model.h"
#include <iostream>
#include <QDebug>
//#include <QString>

class SongStorage : public QObject
{
    Q_OBJECT

    public:
        SongStorage(QQmlPropertyMap *arg);

    public slots:
        void saveSongs();
        void loadSongs();

    private:
        const QString savePath = QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/Music2Dl/";
        const QString saveName = "songs.json";
        QQmlPropertyMap *myMap; 
};
