#ifndef SONGSTORAGE_H
#define SONGSTORAGE_H
#include "songstorage.h"

SongStorage::SongStorage(QQmlPropertyMap *arg) : myMap(arg)
{
    if(!QDir(savePath).exists())
    {
        QDir().mkdir(savePath);
    }
}

void SongStorage::saveSongs()
{
    QJsonObject songLists;

    foreach(QString key, myMap->keys())
    {
        QJsonArray songArray;

        Models::ListModel *temp = QVariant::fromValue((*myMap)[key]).value<Models::ListModel*>();
        QList<Models::ListItem*> songList = temp->toList();

        for(int i = 0; i < songList.size(); i++)
        {
            SongModel *song = (SongModel*) songList.at(i);
            QJsonObject json;

            json["artist"] = song->getArtist();
            json["title"] = song->getTitle();
            json["link"] = song->getLink();

            songArray.append(json);
        }

        songLists[key] = songArray;
    }

    QFile saveFile(savePath + saveName);
    saveFile.open(QIODevice::WriteOnly);
    QJsonDocument doc(songLists);
    saveFile.write(doc.toJson());

    std::cout << "Saved songs" << std::endl;
}

void SongStorage::loadSongs()
{
    QFile loadFile(savePath + saveName);
    if(!loadFile.exists())
    {
        (*myMap)["electrohouse"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["chillstep"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["nu_disco"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["chill"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["chillhop"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["triphop"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["idm"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["dnb"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["glitch"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["jazz"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["classical"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["lounge"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["space"] = QVariant::fromValue(new Models::ListModel(new SongModel()));
        (*myMap)["other"] = QVariant::fromValue(new Models::ListModel(new SongModel()));

        return;
    }
    loadFile.open(QIODevice::ReadOnly);

    QJsonDocument doc(QJsonDocument::fromJson(loadFile.readAll()));

    QJsonObject songLists = (QJsonObject) doc.object();
    foreach(QString key, songLists.keys())
    {
        Models::ListModel *songList = new Models::ListModel(new SongModel());
        QJsonArray songs = songLists[key].toArray();

        qDebug() << key;

        foreach(QJsonValue val, songs)
        {
            QJsonObject obj = val.toObject();
            qDebug() << obj["artist"].toString();
            songList->appendRow(new SongModel(0, obj["artist"].toString(), obj["title"].toString(), obj["link"].toString()));
        }
        myMap->insert(key, QVariant::fromValue(songList));
    }
}
#endif
